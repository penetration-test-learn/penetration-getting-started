### **Awesome Penetration Testing** [![Links Check](https://4.bp.blogspot.com/-_NIGc5XKpSw/WHt9d7wCXaI/AAAAAAAAB0o/OYIv8EWjIoYh44jfxIRSrRYbgrn3MZKEQCLcB/s1600/penetration%2Btesting.png)](http://kalitut.com)
[![10 Common Hacking Techniques](http://img.youtube.com/vi/V3CTfJ2ZP7M/0.jpg)](http://www.youtube.com/watch?v=V3CTfJ2ZP7M "10 Common Hacking Techniques")

A collection of awesome penetration testing resources

- [Online Resources](#online-resources)
  - [Penetration Testing Resources](#penetration-testing-resources)
  - [Exploit development](#exploit-development)
  - [Social Engineering Resources](#social-engineering-resources)
  - [Lock Picking Resources](#lock-picking-resources)
- [Tools](#tools)
  - [Penetration Testing Distributions](#penetration-testing-distributions)
  - [Basic Penetration Testing Tools](#basic-penetration-testing-tools)
  - [Docker for Penetration Testing](#docker-for-penetration-testing)
  - [Vulnerability Scanners](#vulnerability-scanners)
  - [Network Tools](#network-tools)
  - [Wireless Network Tools](#wireless-network-tools)
  - [SSL Analysis Tools](#ssl-analysis-tools)
  - [Web exploitation](#web-exploitation)
  - [Hex Editors](#hex-editors)
  - [Crackers](#crackers)
  - [Windows Utils](#windows-utils)
  - [Linux Utils](#linux-utils)
  - [DDoS Tools](#ddos-tools)
  - [Social Engineering Tools](#social-engineering-tools)
  - [OSInt Tools](#osint-tools)
  - [Anonymity Tools](#anonymity-tools)
  - [Reverse Engineering Tools](#reverse-engineering-tools)
  - [CTF Tools](#ctf-tools)
- [Books](#books)
  - [Penetration Testing Books](#penetration-testing-books)
  - [Hackers Handbook Series](#hackers-handbook-series)
  - [Defensive Development](#defensive-development)
  - [Network Analysis Books](#network-analysis-books)
  - [Reverse Engineering Books](#reverse-engineering-books)
  - [Malware Analysis Books](#malware-analysis-books)
  - [Windows Books](#windows-books)
  - [Social Engineering Books](#social-engineering-books)
  - [Lock Picking Books](#lock-picking-books)
- [Vulnerability Databases](#vulnerability-databases)
- [Security Courses](#security-courses)
- [Information Security Conferences](#information-security-conferences)
- [Information Security Magazines](#information-security-magazines)


### Online Resources
#### Penetration Testing Resources
* [Metasploit Unleashed](https://www.offensive-security.com/metasploit-unleashed/) - Free Offensive Security Metasploit course
* [PTES](http://www.pentest-standard.org/) - Penetration Testing Execution Standard
* [OWASP](https://www.owasp.org/index.php/Main_Page) - Open Web Application Security Project
* [PENTEST-WIKI](https://github.com/nixawk/pentest-wiki) - A free online security knowledge library for pentesters / researchers.
* [Vulnerability Assessment Framework](http://www.vulnerabilityassessment.co.uk/Penetration%20Test.html) - Penetration Testing Framework.
* [The Pentesters Framework](https://github.com/trustedsec/ptf) - PTF attempts to install all of your penetration testing tools (latest and greatest), compile them, build them, and make it so that you can install/update your distribution on any machine. Everything is organized in a fashion that is cohesive to the Penetration Testing Execution Standard (PTES) and eliminates a lot of things that are hardly used.

#### Exploit development
* [Shellcode Tutorial](http://www.vividmachines.com/shellcode/shellcode.html) - Tutorial on how to write shellcode
* [Shellcode Examples](http://shell-storm.org/shellcode/) - Shellcodes database
* [Exploit Writing Tutorials](https://www.corelan.be/index.php/2009/07/19/exploit-writing-tutorial-part-1-stack-based-overflows/) - Tutorials on how to develop exploits
* [shellsploit](https://github.com/b3mb4m/shellsploit-framework) - New Generation Exploit Development Kit
* [Voltron](https://github.com/snare/voltron) - A hacky debugger UI for hackers

#### Social Engineering Resources
* [Social Engineering Framework](http://www.social-engineer.org/framework/general-discussion/) - An information resource for social engineers

#### Lock Picking Resources
* [Schuyler Towne channel](https://www.youtube.com/user/SchuylerTowne/) - Lockpicking videos and security talks
* [/r/lockpicking](https://www.reddit.com/r/lockpicking) - Resources for learning lockpicking, equipment recommendations.

### Tools
#### Penetration Testing Distributions
* [Kali](https://www.kali.org/) - A Linux distribution designed for digital forensics and penetration testing
* [ArchStrike](https://archstrike.org/) - An Arch Linux repository for security professionals and enthusiasts
* [BlackArch](https://www.blackarch.org/) - Arch Linux-based distribution for penetration testers and security researchers
* [NST](http://networksecuritytoolkit.org/) - Network Security Toolkit distribution
* [Pentoo](http://www.pentoo.ch/) - Security-focused livecd based on Gentoo
* [BackBox](https://backbox.org/) - Ubuntu-based distribution for penetration tests and security assessments
* [Parrot](https://www.parrotsec.org/) - A distribution similar to Kali, with multiple architecture
* [Fedora Security Lab](https://labs.fedoraproject.org/en/security/) - Provides a safe test environment to work on security auditing, forensics, system rescue and teaching security testing methodologies.

#### Basic Penetration Testing Tools
* [Metasploit Framework](https://www.metasploit.com/) - World's most used penetration testing software
* [Burp Suite](https://portswigger.net/burp/) - An integrated platform for performing security testing of web applications
* [ExploitPack](http://exploitpack.com/) - Graphical tool for penetration testing with a bunch of exploits
* [BeeF](https://github.com/beefproject/beef) - The Browser Exploitation Framework Project
* [faraday](https://github.com/infobyte/faraday) - Collaborative Penetration Test and Vulnerability Management Platform
* [evilgrade](https://github.com/infobyte/evilgrade) - The update explotation framework
* [commix](https://github.com/stasinopoulos/commix) - Automated All-in-One OS Command Injection and Exploitation Tool
* [routersploit](https://github.com/reverse-shell/routersploit) - Automated penetration testing software for router
* [redsnarf] (https://github.com/nccgroup/redsnarf) - Post-exploitation tool for grabbing credentials

#### Docker for Penetration Testing
* `docker pull kalilinux/kali-linux-docker` [official Kali Linux](https://hub.docker.com/r/kalilinux/kali-linux-docker/)
* `docker pull owasp/zap2docker-stable` - [official OWASP ZAP](https://github.com/zaproxy/zaproxy)
* `docker pull wpscanteam/wpscan` - [official WPScan](https://hub.docker.com/r/wpscanteam/wpscan/)
* `docker pull pandrew/metasploit` - [docker-metasploit](https://hub.docker.com/r/pandrew/metasploit/)
* `docker pull citizenstig/dvwa` - [Damn Vulnerable Web Application (DVWA)](https://hub.docker.com/r/citizenstig/dvwa/)
* `docker pull wpscanteam/vulnerablewordpress` - [Vulnerable WordPress Installation](https://hub.docker.com/r/wpscanteam/vulnerablewordpress/)
* `docker pull hmlio/vaas-cve-2014-6271` - [Vulnerability as a service: Shellshock](https://hub.docker.com/r/hmlio/vaas-cve-2014-6271/)
* `docker pull hmlio/vaas-cve-2014-0160` - [Vulnerability as a service: Heartbleed](https://hub.docker.com/r/hmlio/vaas-cve-2014-0160/)
* `docker pull opendns/security-ninjas` - [Security Ninjas](https://hub.docker.com/r/opendns/security-ninjas/)
* `docker pull diogomonica/docker-bench-security` - [Docker Bench for Security](https://hub.docker.com/r/diogomonica/docker-bench-security/)
* `docker pull ismisepaul/securityshepherd` - [OWASP Security Shepherd](https://hub.docker.com/r/ismisepaul/securityshepherd/)
* `docker pull danmx/docker-owasp-webgoat` - [OWASP WebGoat Project docker image](https://hub.docker.com/r/danmx/docker-owasp-webgoat/)
* `docker-compose build && docker-compose up` - [OWASP NodeGoat](https://github.com/owasp/nodegoat#option-3---run-nodegoat-on-docker)
* `docker pull citizenstig/nowasp` - [OWASP Mutillidae II Web Pen-Test Practice Application](https://hub.docker.com/r/citizenstig/nowasp/)
* `docker pull bkimminich/juice-shop` - [OWASP Juice Shop](https://github.com/bkimminich/juice-shop#docker-container--)

#### Vulnerability Scanners
* [Nexpose](https://www.rapid7.com/products/nexpose/) - Vulnerability Management & Risk Management Software
* [Nessus](http://www.tenable.com/products/nessus-vulnerability-scanner) - Vulnerability, configuration, and compliance assessment
* [Nikto](https://cirt.net/nikto2) - Web application vulnerability scanner
* [OpenVAS](http://www.openvas.org/) - Open Source vulnerability scanner and manager
* [OWASP Zed Attack Proxy](https://www.owasp.org/index.php/OWASP_Zed_Attack_Proxy_Project) - Penetration testing tool for web applications
* [Secapps](https://secapps.com/) - Integrated web application security testing environment
* [w3af](https://github.com/andresriancho/w3af) - Web application attack and audit framework
* [Wapiti](http://wapiti.sourceforge.net/) - Web application vulnerability scanner
* [WebReaver](http://www.webreaver.com/) - Web application vulnerability scanner for Mac OS X
* [DVCS Ripper](https://github.com/kost/dvcs-ripper) - Rip web accessible (distributed) version control systems: SVN/GIT/HG/BZR
* [arachni](https://github.com/Arachni/arachni) - Web Application Security Scanner Framework

#### Network Tools
* [nmap](https://nmap.org/) - Free Security Scanner For Network Exploration & Security Audits
* [pig](https://github.com/rafael-santiago/pig) - A Linux packet crafting tool
* [tcpdump/libpcap](http://www.tcpdump.org/) - A common packet analyzer that runs under the command line
* [Wireshark](https://www.wireshark.org/) - A network protocol analyzer for Unix and Windows
* [Network Tools](http://network-tools.com/) - Different network tools: ping, lookup, whois, etc
* [netsniff-ng](https://github.com/netsniff-ng/netsniff-ng) - A Swiss army knife for for network sniffing
* [Intercepter-NG](http://sniff.su/) - a multifunctional network toolkit
* [SPARTA](http://sparta.secforce.com/) - Network Infrastructure Penetration Testing Tool
* [dnschef](http://thesprawl.org/projects/dnschef/) - A highly configurable DNS proxy for pentesters
* [DNSDumpster](https://dnsdumpster.com/) - Online DNS recon and search service
* [dnsenum](https://github.com/fwaeytens/dnsenum/) - Perl script that enumerates DNS information from a domain, attempts zone transfers, performs a brute force dictionary style attack, and then performs reverse look-ups on the results
* [dnsmap](https://github.com/makefu/dnsmap/) - Passive DNS network mapper
* [dnsrecon](https://github.com/darkoperator/dnsrecon/) - DNS Enumeration Script
* [dnstracer](http://www.mavetju.org/unix/dnstracer.php) - Determines where a given DNS server gets its information from, and follows the chain of DNS servers
* [passivedns-client](https://github.com/chrislee35/passivedns-client) - Provides a library and a query tool for querying several passive DNS providers
* [passivedns](https://github.com/gamelinux/passivedns) - A network sniffer that logs all DNS server replies for use in a passive DNS setup
* [Mass Scan](https://github.com/robertdavidgraham/masscan) - TCP port scanner, spews SYN packets asynchronously, scanning entire Internet in under 5 minutes.
* [Zarp](https://github.com/hatRiot/zarp) - Zarp is a network attack tool centered around the exploitation of local networks
* [mitmproxy](https://github.com/mitmproxy/mitmproxy) - An interactive SSL-capable intercepting HTTP proxy for penetration testers and software developers
* [mallory](https://github.com/justmao945/mallory) - HTTP/HTTPS proxy over SSH
* [Netzob](https://github.com/netzob/netzob) - Reverse engineering, traffic generation and fuzzing of communication protocols
* [DET](https://github.com/sensepost/DET) - DET is a proof of concept to perform Data Exfiltration using either single or multiple channel(s) at the same time
* [pwnat](https://github.com/samyk/pwnat) - punches holes in firewalls and NATs
* [dsniff](https://www.monkey.org/~dugsong/dsniff/) - a collection of tools for network auditing and pentesting
* [tgcd](http://tgcd.sourceforge.net/) - a simple Unix network utility to extend the accessibility of TCP/IP based network services beyond firewalls
* [smbmap](https://github.com/ShawnDEvans/smbmap) - a handy SMB enumeration tool
* [scapy](https://github.com/secdev/scapy) - a python-based interactive packet manipulation program & library
* [Dshell](https://github.com/USArmyResearchLab/Dshell) - Network forensic analysis framework
* [Debookee (MAC OS X)](http://www.iwaxx.com/debookee/) - Intercept traffic from any device on your network
* [Dripcap](https://github.com/dripcap/dripcap) - Caffeinated packet analyzer

#### Wireless Network Tools
* [Aircrack-ng](http://www.aircrack-ng.org/) - a set of tools for auditing wireless network
* [Kismet](https://kismetwireless.net/) - Wireless network detector, sniffer, and IDS
* [Reaver](https://code.google.com/archive/p/reaver-wps) - Brute force attack against Wifi Protected Setup
* [Wifite](https://github.com/derv82/wifite) - Automated wireless attack tool
* [wifiphisher](https://github.com/sophron/wifiphisher) - Automated phishing attacks against Wi-Fi networks

#### SSL Analysis Tools
* [SSLyze](https://github.com/nabla-c0d3/sslyze) - SSL configuration scanner
* [sslstrip](https://www.thoughtcrime.org/software/sslstrip/) - a demonstration of the HTTPS stripping attacks
* [sslstrip2](https://github.com/LeonardoNve/sslstrip2) - SSLStrip version to defeat HSTS
* [tls_prober](https://github.com/WestpointLtd/tls_prober) - fingerprint a server's SSL/TLS implementation

#### Web exploitation
* [WPScan](https://wpscan.org/) - Black box WordPress vulnerability scanner
* [SQLmap](http://sqlmap.org/) - Automatic SQL injection and database takeover tool
* [weevely3](https://github.com/epinna/weevely3) - Weaponized web shell
* [Wappalyzer](https://wappalyzer.com/) - Wappalyzer uncovers the technologies used on websites
* [cms-explorer](https://code.google.com/archive/p/cms-explorer/) - CMS Explorer is designed to reveal the the specific modules, plugins, components and themes that various CMS driven web sites are running.
* [joomscan](https://www.owasp.org/index.php/Category:OWASP_Joomla_Vulnerability_Scanner_Project) - Joomla CMS scanner
* [WhatWeb](https://github.com/urbanadventurer/WhatWeb) - Website Fingerprinter
* [BlindElephant](http://blindelephant.sourceforge.net/) - Web Application Fingerprinter
* [fimap](https://github.com/kurobeats/fimap) - Find, prepare, audit, exploit and even google automatically for LFI/RFI bugs
* [Kadabra](https://github.com/D35m0nd142/Kadabra) - Automatic LFI exploiter and scanner
* [Kadimus](https://github.com/P0cL4bs/Kadimus) - LFI scan and exploit tool
* [liffy](https://github.com/hvqzao/liffy) - LFI exploitation tool

#### Hex Editors
* [HexEdit.js](https://hexed.it) - Browser-based hex editing
* [Hexinator](https://hexinator.com/) (commercial) - World's finest Hex Editor
* [HxD - Freeware Hex Editor and Disk Editor](https://mh-nexus.de/en/hxd/)


#### Crackers
* [John the Ripper](http://www.openwall.com/john/) - Fast password cracker
* [Online MD5 cracker](http://www.md5crack.com/) - Online MD5 hash Cracker
* [Hashcat](http://hashcat.net/hashcat/) - The more fast hash cracker
* [THC Hydra](http://sectools.org/tool/hydra/) - Another Great Password Cracker

#### Windows Utils
* [Sysinternals Suite](https://technet.microsoft.com/en-us/sysinternals/bb842062) - The Sysinternals Troubleshooting Utilities
* [Windows Credentials Editor](http://www.ampliasecurity.com/research/windows-credentials-editor/) - security tool to list logon sessions and add, change, list and delete associated credentials
* [mimikatz](http://blog.gentilkiwi.com/mimikatz) - Credentials extraction tool for Windows OS
* [PowerSploit](https://github.com/PowerShellMafia/PowerSploit) - A PowerShell Post-Exploitation Framework
* [Windows Exploit Suggester](https://github.com/GDSSecurity/Windows-Exploit-Suggester) - Detects potential missing patches on the target
* [Responder](https://github.com/SpiderLabs/Responder) - A LLMNR, NBT-NS and MDNS poisoner
* [Bloodhound](https://github.com/adaptivethreat/Bloodhound/wiki) - A graphical Active Directory trust relationship explorer
* [Empire](https://github.com/PowerShellEmpire/Empire) - Empire is a pure PowerShell post-exploitation agent
* [Fibratus](https://github.com/rabbitstack/fibratus) - Tool for exploration and tracing of the Windows kernel

#### Linux Utils
* [Linux Exploit Suggester](https://github.com/PenturaLabs/Linux_Exploit_Suggester) - Linux Exploit Suggester; based on operating system release number.

#### DDoS Tools
* [LOIC](https://github.com/NewEraCracker/LOIC/) - An open source network stress tool for Windows
* [JS LOIC](http://metacortexsecurity.com/tools/anon/LOIC/LOICv1.html) - JavaScript in-browser version of LOIC
* [T50](https://sourceforge.net/projects/t50/) - The more fast network stress tool

#### Social Engineering Tools
* [SET](https://github.com/trustedsec/social-engineer-toolkit) - The Social-Engineer Toolkit from TrustedSec

#### OSInt Tools
* [Maltego](http://www.paterva.com/web7/) - Proprietary software for open source intelligence and forensics, from Paterva.
* [theHarvester](https://github.com/laramies/theHarvester) - E-mail, subdomain and people names harvester
* [creepy](https://github.com/ilektrojohn/creepy) - A geolocation OSINT tool
* [metagoofil](https://github.com/laramies/metagoofil) - Metadata harvester
* [Google Hacking Database](https://www.exploit-db.com/google-hacking-database/) - a database of Google dorks; can be used for recon
* [Censys](https://www.censys.io/) - Collects data on hosts and websites through daily ZMap and ZGrab scans
* [Shodan](https://www.shodan.io/) - Shodan is the world's first search engine for Internet-connected devices
* [recon-ng](https://bitbucket.org/LaNMaSteR53/recon-ng) - A full-featured Web Reconnaissance framework written in Python
* [github-dorks](https://github.com/techgaun/github-dorks) - CLI tool to scan github repos/organizations for potential sensitive information leak
* [vcsmap](https://github.com/melvinsh/vcsmap) - A plugin-based tool to scan public version control systems for sensitive information
* [Spiderfoot](http://www.spiderfoot.net/) - multi-source OSINT automation tool with a Web UI and report visualizations

#### Anonymity Tools
* [Tor](https://www.torproject.org/) - The free software for enabling onion routing online anonymity
* [I2P](https://geti2p.net/en/) - The Invisible Internet Project
* [Nipe](https://github.com/GouveaHeitor/nipe) - Script to redirect all traffic from the machine to the Tor network.

#### Reverse Engineering Tools
* [IDA Pro](https://www.hex-rays.com/products/ida/) - A Windows, Linux or Mac OS X hosted multi-processor disassembler and debugger
* [IDA Free](https://www.hex-rays.com/products/ida/support/download_freeware.shtml) - The freeware version of IDA v5.0
* [WDK/WinDbg](https://msdn.microsoft.com/en-us/windows/hardware/hh852365.aspx) - Windows Driver Kit and WinDbg
* [OllyDbg](http://www.ollydbg.de/) - An x86 debugger that emphasizes binary code analysis
* [Radare2](http://rada.re/r/index.html) - Opensource, crossplatform reverse engineering framework
* [x64_dbg](http://x64dbg.com/) - An open-source x64/x32 debugger for windows
* [Immunity Debugger](http://debugger.immunityinc.com/) - A powerful new way to write exploits and analyze malware
* [Evan's Debugger](http://www.codef00.com/projects#debugger) - OllyDbg-like debugger for Linux
* [Medusa disassembler](https://github.com/wisk/medusa) - An open source interactive disassembler
* [plasma](https://github.com/joelpx/plasma) - Interactive disassembler for x86/ARM/MIPS. Generates indented pseudo-code with colored syntax code
* [peda](https://github.com/longld/peda) - Python Exploit Development Assistance for GDB
* [dnSpy](https://github.com/0xd4d/dnSpy) - dnSpy is a tool to reverse engineer .NET assemblies

#### CTF Tools
* [Pwntools](https://github.com/Gallopsled/pwntools) - CTF framework for use in CTFs
